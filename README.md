## Challenge 12

#UFO Website Database

#Overview
We designed a website to allow people to search through some UFO information based on several factors such as location and shape and date. This will allow people to drill down into the data with ease. 

# Results
By adding any number of search criteria users can see such information as if a certain type of ship likes different locations by searching for a certain type of ship. Or possibly one could find out if ships were seen in different places for several days, by searching day by day as well as type of ship.

# Summary
I think the main limitation is the inability to visualize the data. You could improve this by adding a feature that plots the sightings on a map, or add a sliding bar to give a window of time to be able to search the duration. 
